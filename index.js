const dotenv = require("dotenv");
const http = require("https");
const PouchDB = require("pouchdb");
const winston = require("winston");

const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  defaultMeta: { service: "wilder-node" },
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new winston.transports.File({ filename: "error.log", level: "error" }),
    new winston.transports.File({ filename: "combined.log" }),
  ],
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== "production") {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    })
  );
}

// Read the .env file and place its contents in the `process.env` object
dotenv.config();

// Creates the PouchDB "donations", and the document "paid".
// Returns the document if already exists, or creates it otherwise
// This function returns a Promise({ doc, db })
//
// The Document has the following structure:
// { _id: "paid",
//   donors: new Map(),
//   donations: new Map(),
//   processed: new Map()
// }
function createDB() {
  return new Promise((resolve, reject) => {
    const db = new PouchDB("donations");
    db.info().then(function (info) {
      logger.log("info", "Starting DB", new Date().toISOString());
      logger.log("info", info);
    });
    db.get("paid")
      .then((doc) => {
        logger.log("info", "Using existing 'paid' document");
        resolve({ doc, db });
      })
      .catch((err) => {
        if (err.name === "not_found") {
          // Create the initial version of the DOC
          logger.log({
            level: "info",
            message: 'Creating a new "paid" document',
          });
          // A document can only hold standard JS objects, it does not
          // support Map() or Set()
          db.put({
            _id: "paid",
            donors: {},
            donations: {},
            processed: {},
            invalid: {},
          });
          db.get("paid").then((doc) => resolve({ doc, db }));
        } else {
          // hm, some other error
          // Create the initial version of the DOC
          logger.log(
            "error",
            `Error accessing "paid" document in PouchDB - ${new Date().toISOString()}`
          );
          logger.log("error", `Error Data`, err);
        }
      });
  });
}

/**
 This is the entry point of the program flow.
*/
createDB().then(({ db }) => {
  setInterval(() => {
    db.get("paid").then((doc) => {
      getDonations().then(({ donations }) => {
        // Process each donation JSON into the DB.
        processDonations(donations, doc).then(async (updatedDoc) => {
          for (const id of Object.keys(updatedDoc.donations)) {
            if (
              !updatedDoc.processed[id] &&
              (updatedDoc.donations[id].status.includes("Concl") ||
                updatedDoc.donations[id].status.includes("Renova"))
            ) {
              // Create an Invoice
              const donation = updatedDoc.donations[id];
              const donor = updatedDoc.donors[donation.email];
              try {
                const invoice = await createInvoice(donation, donor);
                if (invoice.errors && invoice.errors.length > 0) {
                  for (const error of invoice.errors) {
                    logger.log(
                      "error",
                      `Invoice Error for Donation ${id} - Donor ${
                        donor.id
                      } - ${new Date().toISOString()}`
                    );
                    logger.log(
                      "error",
                      `Invoice Error Data for Donation ${id}`,
                      error
                    );
                  }
                  updatedDoc.invalid[id] = { donation, donor, invoice };
                  // Don't mark the donation as processed
                  continue;
                }
                logger.log(
                  "info",
                  `Invoice ${
                    invoice.invoice_receipt.id
                  } - ${new Date().toISOString()}`
                );
                logger.log(
                  "info",
                  `Invoice Data ${invoice.invoice_receipt.id}`,
                  invoice
                );
                // Add the donation to the object of processed donations
                updatedDoc.processed[id] = donation;
                // Send it to the client by email
                await changeInvoiceState(invoice.invoice_receipt);
                await sendInvoiceByMail(invoice.invoice_receipt);
              } catch (error) {
                console.error("Error processing donation", error);
                updatedDoc.invalid[id] = { donation, donor, invoice };
              }
            }
          }
          // Add the updated doc to the PouchDB
          db.put(doc);
        });
      });
    });
  }, 40000);
});

/**
 * Recieves a donations raw JSON from the GiveWP API and transforms it into a database doc.
 */
const processDonations = (donations, doc) => {
  return new Promise((resolve, reject) => {
    if (!donations || donations.length === 0) {
      resolve(doc);
      return;
    }
    // For each donation
    for (const donation of donations) {
      // * Add it to the doc if it is not present there
      if (!doc.donations[donation.number]) {
        doc.donations[donation.number] = donation;
        logger.log(
          "info",
          `Donation ${donation.number} - ${new Date().toISOString()}`
        );
        logger.log("info", `Donation Data ${donation.number}`, donation);
      }
      // * Add the donor to the doc if it is not present there
      if (!doc.donors[donation.email]) {
        doc.donors[donation.email] = {
          email: donation.email,
          name: donation.name,
          fname: donation.fname,
          lname: donation.lname,
          address1: donation.payment_meta._give_donor_billing_address1,
          address2: donation.payment_meta._give_donor_billing_address2,
          city: donation.payment_meta._give_donor_billing_city,
          state: donation.payment_meta._give_donor_billing_state,
          zip: donation.payment_meta._give_donor_billing_zip,
          country: donation.payment_meta._give_donor_billing_country,
          stripe_source_id: donation.payment_meta._give_stripe_source_id,
          stripe_customer_id: donation.payment_meta._give_stripe_customer_id,
          vat: donation.payment_meta.eu_vat,
          id: donation.payment_meta._give_payment_donor_id,
        };
      }
    }
    resolve(doc);
  });
};

/**
 * Access the donations API and returns the JSON without any processing being done in it.
 * 
 * Each donation has the following structure:
 {
    ID: 49959,
    number: '133',
    transaction_id: '...',
    key: '...',
    total: '4,00',
    status: 'Concluído',
    gateway: 'stripe_sepa',
    name: 'A B C',
    fname: 'A',
    lname: 'B C',
    email: 'abc@example.com',
    date: '2022-05-24 11:32:20',
    payment_meta: {
      _give_payment_currency: 'EUR',
      _give_donor_billing_first_name: 'A',
      _give_donor_billing_last_name: 'B C',
      _give_donor_billing_address1: 'Rua XPT',
      _give_donor_billing_address2: '59',
      _give_donor_billing_city: 'Amadora',
      _give_donor_billing_state: '',
      _give_donor_billing_zip: '0000-000',
      _give_donor_billing_country: 'PT',
      _give_payment_gateway: 'stripe_sepa',
      _give_payment_donor_title_prefix: '',
      _give_payment_donor_email: 'abc@example.com',
      _give_payment_donor_ip: 'xxx.xxx.xxx.xxx',
      _give_payment_mode: 'live',
      _give_payment_donor_id: '00',
      _give_is_donation_recurring: '1',
      _give_current_url: 'https://www.wilder.pt/contribua/',
      _give_current_page_id: '37815',
      _give_anonymous_donation: '0',
      _give_stripe_account_slug: 'xxxxx',
      eu_vat: '000000000',
      _give_stripe_source_id: 'xxxxx',
      _give_stripe_customer_id: 'xxxx',
      _give_subscription_payment: '1',
      _give_payment_transaction_id: 'xxxx',
      _give_completed_date: '2022-05-27 03:07:30'
    },
    form: {
      id: '37811',
      name: 'Contribua',
      price: '0,00',
      price_name: '€50,00',
      price_id: 'custom'
    }
  }
 */
function getDonations(lastDonation) {
  return new Promise((resolve, reject) => {
    const { SITEURL, GIVEWP_ENDPOINT, GIVEWP_PUB_KEY, GIVEWP_TOKEN } =
      process.env;
    const url = `https://${SITEURL}/${GIVEWP_ENDPOINT}donations/?key=${GIVEWP_PUB_KEY}&token=${GIVEWP_TOKEN}`;
    var chunks = [];
    http
      .get(url, (res) => {
        if (res.statusCode !== 200) {
          logger.log(
            "error",
            `Status Code ${res.statusCode} when reading givewp donations`,
            res.headers,
            new Date().toISOString()
          );
        }
        res.on("data", (chunk) => {
          chunks.push(chunk);
        });
        res.on("end", () => {
          var body = Buffer.concat(chunks);
          resolve(JSON.parse(body.toString()));
        });
      })
      .on("error", (e) => {
        logger.log(
          "error",
          `Reading givewp donations - ${new Date().toISOString()}`,
          e
        );
        reject(e);
      });
  });
}

// Gets a date string in the format: YYYY-MM-DD and formats it to be
// DD/MM/YYYY
// Optionally it adds days to it (useful to send on the invoice due date)
function dateFormat(dateString, daysToAdd = 0) {
  const date = new Date(dateString.split(" ")[0]);
  date.setDate(date.getDate() + daysToAdd);
  return date.toISOString().split("T")[0].split("-").reverse().join("/");
}

function createInvoice(donation, donor) {
  return new Promise((resolve, reject) => {
    var options = {
      method: "POST",
      hostname: `${process.env.INVOICEXPRESS_ACCOUNT}.app.invoicexpress.com`,
      port: null,
      path: `/invoice_receipts.json?api_key=${process.env.INVOICEXPRESS_KEY}`,
      headers: {
        accept: "application/json",
        "content-type": "application/json",
      },
    };

    var req = http.request(options, function (res) {
      var chunks = [];

      res.on("data", function (chunk) {
        chunks.push(chunk);
      });

      res.on("end", function () {
        var body = Buffer.concat(chunks);
        try {
          const responseObj = JSON.parse(body.toString());
          resolve(responseObj);
        } catch (e) {
          logger.log(
            "error",
            `Parsing invoice response for donation ${donation.id} - donor: ${
              donor.id
            } - ${new Date().toISOString()}`
          );
          logger.log("error", `Parsing error data`, e);
          reject(e);
        }
      });
    });

    const total = Number(donation.total.replace(",", "."));
    const minimumDonation = 5;
    const unitPrice = minimumDonation / 1.06; // 6% IVA tax
    const quantity = total / minimumDonation;
    let obs = donation.ID
      ? `Contribuição ${donation.number} (id: ${donation.ID})`
      : "Contribuição";

    if (donation.subscription_id !== undefined) {
      obs += ` - Renovação mensal (id: ${donation.subscription_id})`;
    }

    const invoice = {
      invoice: {
        date: dateFormat(donation.date),
        due_date: dateFormat(donation.date, 7),
        observations: obs,
        client: {
          name: donor.name,
          code: donor.ir,
          email: donor.email,
          address: `${donor.address1} ${donor.address2}`,
          city: donor.city,
          postal_code: donor.zip,
          country: donor.country === "PT" ? "Portugal" : undefined,
          fiscal_id: donor.vat,
        },
        items: [
          {
            name: "Contributo",
            description: "Contribuição Wilder",
            unit_price: String(unitPrice),
            quantity: String(quantity),
            tax: {
              name: "IVA6",
              value: 6,
            },
          },
        ],
      },
    };

    req.write(JSON.stringify(invoice));
    req.end();
  });
}

function sendInvoiceByMail(invoice) {
  return new Promise((resolve, reject) => {
    var options = {
      method: "PUT",
      hostname: `${process.env.INVOICEXPRESS_ACCOUNT}.app.invoicexpress.com`,
      port: null,
      path: `/invoice_receipts/${invoice.id}/email-document.json?api_key=${process.env.INVOICEXPRESS_KEY}`,
      headers: {
        accept: "application/json",
        "content-type": "application/json",
      },
    };

    var req = http.request(options, function (res) {
      var chunks = [];

      res.on("data", function (chunk) {
        chunks.push(chunk);
      });

      res.on("end", function () {
        var body = Buffer.concat(chunks);
        const responseStr = body.toString();
        resolve(responseStr);
      });
    });

    setTimeout(() => {
      req.write(
        JSON.stringify({
          message: {
            client: { email: invoice.client.email, save: "0" },
            subject: "Fatura/Recibo da Wilder",
            body: "Obrigada por fazer parte da Wilder. Poderá fazer download do PDF do documento no link que se segue.",
            logo: "1",
          },
        })
      );
      req.end();
    }, 300);
  });
}

function changeInvoiceState(invoice) {
  return new Promise((resolve, reject) => {
    var options = {
      method: "PUT",
      hostname: `${process.env.INVOICEXPRESS_ACCOUNT}.app.invoicexpress.com`,
      port: null,
      path: `/invoice_receipts/${invoice.id}/change-state.json?api_key=${process.env.INVOICEXPRESS_KEY}`,
      headers: {
        accept: "application/json",
        "content-type": "application/json",
      },
    };

    var req = http.request(options, function (res) {
      var chunks = [];

      res.on("data", function (chunk) {
        chunks.push(chunk);
      });

      res.on("end", function () {
        var body = Buffer.concat(chunks);
        const responseStr = body.toString();
        resolve(responseStr);
      });
      res.on("error", function () {
        console.error("HTTP error when changing invoice state.", invoice);
      });
    });

    req.write(JSON.stringify({ invoice: { state: "finalized" } }));
    req.end();
  });
}
