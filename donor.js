/**
  * This code updates the donor NIF/VAT
  * Run it like: node donor.js "donor@email.com" 234133406
  */
const PouchDB = require("pouchdb")

const donorId = process.argv.slice(2)[0];
const nif = process.argv.slice(2)[1];
console.log('Updating Donor', donorId, 'With NIF', nif)

const db = new PouchDB('donations');
db.get('paid').then((doc) => {
  doc.donors[donorId].vat = nif
  db.put(doc)
})
